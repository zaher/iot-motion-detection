# IoT Motion Detection 

[![MicrosoftTeams-image__4_](/uploads/4872c04f7b01c0683051bf7b0bd6c9a8/MicrosoftTeams-image__4_.png)](https://videos.simpleshow.com/updPhh44yY)

## Kurze Vorstellung des Projektes

[![image](/uploads/48bf4817a9236a9c9a86d9e0fcd3db56/image.png)](https://gitlab.reutlingen-university.de/zaher/iot-motion-detection/-/blob/main/IoT_WS2021_22_Balaban_Karaman_Zaher.pptx)

## Projektdokumentation

Hier kommt Ihr zu unserem [Wiki](https://gitlab.reutlingen-university.de/zaher/iot-motion-detection/-/wikis/home).
